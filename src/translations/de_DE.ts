<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_US">
<context>
    <name>DeviceDialog</name>
    <message>
        <location filename="../DeviceDialog.ui" line="20"/>
        <source>USBGuard Device Dialog</source>
        <translation>USBGuard Gerätedialog</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="53"/>
        <source>Serial #:</source>
        <translation>Seriennummer:</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="104"/>
        <location filename="../DeviceDialog.ui" line="118"/>
        <location filename="../DeviceDialog.ui" line="164"/>
        <source>TextLabel</source>
        <translation>Textlabel</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="131"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="147"/>
        <source>Device ID:</source>
        <translation>Geräte ID:</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="187"/>
        <source>Make the decision permanent</source>
        <translation>Treffe die Entscheidung permanent</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="583"/>
        <location filename="../DeviceDialog.cpp" line="202"/>
        <source>Block</source>
        <translation>Blockieren</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="966"/>
        <location filename="../DeviceDialog.cpp" line="197"/>
        <source>Allow</source>
        <translation>Erlauben</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="1349"/>
        <location filename="../DeviceDialog.cpp" line="212"/>
        <source>Reject</source>
        <translation>Ablehnen</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.ui" line="1361"/>
        <source>(Press Escape to stop the countdown)</source>
        <translation>(Drücke ESC um den Countdown zu stoppen)</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.cpp" line="39"/>
        <source>USB Device Inserted</source>
        <translation>USB Gerät eingesteckt</translation>
    </message>
    <message>
        <location filename="../DeviceDialog.cpp" line="220"/>
        <source>(Press Escape to close this window)</source>
        <translation>(Drücke ESC um dieses Fenster zu schließen)</translation>
    </message>
</context>
<context>
    <name>DeviceModel</name>
    <message>
        <location filename="../DeviceModel.cpp" line="187"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../DeviceModel.cpp" line="197"/>
        <source> M </source>
        <translation> M </translation>
    </message>
    <message>
        <location filename="../DeviceModel.cpp" line="208"/>
        <source>Target</source>
        <translation>Ziel</translation>
    </message>
    <message>
        <location filename="../DeviceModel.cpp" line="219"/>
        <source>USB ID</source>
        <translation>USB ID</translation>
    </message>
    <message>
        <location filename="../DeviceModel.cpp" line="230"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../DeviceModel.cpp" line="237"/>
        <source>Serial</source>
        <translation>Seriennummer</translation>
    </message>
    <message>
        <location filename="../DeviceModel.cpp" line="244"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../DeviceModel.cpp" line="251"/>
        <source>Interfaces</source>
        <translation>Interfaces</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Hauptfenster</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="31"/>
        <source>Devices</source>
        <translation>Geräte</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="37"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="60"/>
        <source>Apply</source>
        <translation>Übernehmen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="121"/>
        <source>Permanently</source>
        <translation>Permanent</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="129"/>
        <source>Messages</source>
        <translation>Nachrichten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="146"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="164"/>
        <source>Notifications</source>
        <translation>Benachrichtigungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="170"/>
        <source>Allowed</source>
        <translation>Erlaubt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="177"/>
        <source>Removed</source>
        <translation>Entfernt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="184"/>
        <source>Rejected</source>
        <translation>Abgelehnt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="194"/>
        <source>Inserted</source>
        <translation>Eingesteckt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="217"/>
        <source>Blocked</source>
        <translation>Blockiert</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="227"/>
        <source>IPC status</source>
        <translation>IPC Status</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="234"/>
        <source>Present</source>
        <translation>Anwesend</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="250"/>
        <source>Device Dialog</source>
        <translation>Gerätedialog</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="263"/>
        <source>Exit to tray</source>
        <translation>Beende zu Tray</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="352"/>
        <source>Decision is permanent by default</source>
        <translation>Entscheidung ist standardmäßig permanent</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="277"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reject causes the device to be logically removed from the operating system. The device might still get power from the from the bus, however, it has to be reinserted for it to get visible for the operating system again.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ablehnen sorgt dafür, dass das Gerät logisch vom Betriebssystem entfernt wird. Das Gerät wird eventuell noch vom Bus mit Energie versorgt, muss aber neu eingesteckt werden um für das Betriebssystem wieder sichtbar zu werden&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="280"/>
        <source>Show the Reject button</source>
        <translation>Zeige die Ablehnen Schaltfläche</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="319"/>
        <source>Default decision timeout (seconds)</source>
        <translation>Entscheidungstimeout (Sekunden)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="270"/>
        <source>Decision method</source>
        <translation>Entscheidungsmethode</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <source>Default decision</source>
        <translation>Standardentscheidung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="360"/>
        <source>allow</source>
        <translation>erlauben</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="365"/>
        <source>block</source>
        <translation>blockieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="370"/>
        <source>reject</source>
        <translation>ablehnen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="311"/>
        <source>Buttons</source>
        <translation>Schaltflächen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="287"/>
        <source>Randomize position of the window</source>
        <translation>Wähle zufällige Position auf Bildschirm</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="339"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Starting from the end, mask every 2nd character in the displayed serial number value with an asterisk symbol.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Beginnend vom Ende wird jedes zweite Zeichen in der dargestellten Seriennummer durch ein Asterisk Symbol ersetzt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="342"/>
        <source>Mask the serial number value</source>
        <translation>Maskiere die Seriennummer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="378"/>
        <source>Save window size</source>
        <translation>Fenstergröße speichern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="395"/>
        <location filename="../MainWindow.cpp" line="110"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="94"/>
        <source>Inactive. No IPC connection.</source>
        <translation>Inaktiv. Keine IPC Verbindung.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="278"/>
        <source>USB Device Inserted</source>
        <translation>USB Gerät eingsteckt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="283"/>
        <source>USB Device Updated</source>
        <translation>USB Gerät aktualisiert</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="287"/>
        <source>USB Device Removed</source>
        <translation>USB Gerät entfernt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="292"/>
        <source>USB Device Present</source>
        <translation>USB Gerät anwesend</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="313"/>
        <source>USB Device Allowed</source>
        <translation>USB Gerät erlaubt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="318"/>
        <source>USB Device Blocked</source>
        <translation>USB Gerät blockiert</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="324"/>
        <source>USB Device Rejected</source>
        <translation>USB Gerät abgelehnt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="373"/>
        <source>IPC Connection Established</source>
        <translation>IPC Verbindung etabliert</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="384"/>
        <source>IPC Connection Lost</source>
        <translation>IPC Verbindung verloren</translation>
    </message>
</context>
<context>
    <name>TargetDelegate</name>
    <message>
        <location filename="../TargetDelegate.cpp" line="42"/>
        <source>allow</source>
        <translation>erlauben</translation>
    </message>
    <message>
        <location filename="../TargetDelegate.cpp" line="43"/>
        <source>block</source>
        <translation>blockieren</translation>
    </message>
    <message>
        <location filename="../TargetDelegate.cpp" line="44"/>
        <source>reject</source>
        <translation>ablehnen</translation>
    </message>
</context>
</TS>
