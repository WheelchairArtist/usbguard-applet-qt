#Deprecate QT API versions older then 6.3
DEFINES += QT_DISABLE_DEPRECATED_UP_TO=0x060300

CONFIG += qt

QT += core widgets

LIBS += -lusbguard

SOURCES += DeviceDialog.cpp
SOURCES += DeviceModel.cpp
SOURCES += MainWindow.cpp
SOURCES += SessionBlocker.cpp
SOURCES += TargetDelegate.cpp
SOURCES += main.cpp

FORMS += DeviceDialog.ui
FORMS += MainWindow.ui

HEADERS += ui_DeviceDialog.h
HEADERS += ui_MainWindow.h

HEADERS += DeviceDialog.h
HEADERS += DeviceModel.h
HEADERS += MainWindow.h
HEADERS += SessionBlocker.h
HEADERS += TargetDelegate.h

RESOURCES += usbguard-qt.qrc

TRANSLATIONS += translations/cs_CZ.ts
TRANSLATIONS += translations/de_DE.ts
TRANSLATIONS += translations/es_AR.ts